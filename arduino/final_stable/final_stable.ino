

/***   Included libraries  ***/

#include <BitbloqZowi.h>
#include <BitbloqZowiSerialCommand.h>
#include <BitbloqUS.h>
#include <BitbloqBatteryReader.h>
#include <BitbloqLedMatrix.h>
#include <Servo.h>
#include <BitbloqOscillator.h>
#include <EEPROM.h>

/***   Global variables and function definition  ***/
Zowi zowi;

int default_mouth = happyOpen;

int side_walk_steps = 3;
int side_walk_freq = 800;
int side_walk_height = 25;

int front_walk_steps = 2;
int front_walk_freq = 1000;

bool shake_leg = false; // Whether to shake leg after raising it
int raise_leg_freq = 2000;

bool play_sound = true; // Whether to play sound after any sound after each action
int movement_delay_sound = 100; // Milliseconds to wait after the movement to play sound
int face_delay_sound = 2000; // Milliseconds to wait a


/***   List of available commands ***/

#define WALK_FORWARD    "walk_forward"
#define WALK_BACKWARDS  "walk_backwards"
#define WALK_LEFT       "walk_left"
#define WALK_RIGHT      "walk_right"
#define RAISE_LEFT      "raise_left"
#define RAISE_RIGHT     "raise_right"
#define HAPPY_FACE      "happy_face"
#define SAD_FACE        "sad_face"
#define ANGRY_FACE      "angry_face"
#define COUNTDOWN_ANIM  "countdown_anim"
#define CORRECT_ANIM    "correct_anim"
#define WIN_ANIM        "win_anim"
#define FAIL_ANIM       "fail_anim"


void send_movement_ended() {
  Serial.print("ok\n");
}


/*** Move functions ***/

void walk_forwards() {
  walk_front(FORWARD);
}

void walk_backwards(){
  walk_front(BACKWARD);
}

void walk_front(int dir) {
  zowi.walk(front_walk_steps, front_walk_freq, dir);
  end_movement();
}

// Resets initial position and sends completion message
void end_movement() {
    zowi.home(); // Return to initial position
    if (play_sound) {
      delay(movement_delay_sound);
      // Play movement as end of sequence move
      zowi.sing(S_happy_short);
    }
    delay(600);
    send_movement_ended();
}

void walk_left() {
  walk_side(LEFT);
}

void walk_right() {
  walk_side(RIGHT);
}

void walk_side(int dir) {
  zowi.moonwalker(side_walk_steps, side_walk_freq, side_walk_height, dir);
  end_movement();
}


void raise_left() {
  raise_foot(RIGHT); // It's opposite
}

void raise_right() {
  raise_foot(LEFT); // It's opposite
}

void raise_foot(int dir) {
  if (shake_leg) {
    zowi.shakeLeg(1, raise_leg_freq, dir);
  }
  else {
    zowi.bend(1, raise_leg_freq, dir);
  }
  end_movement();
}


/*** Sound functions ***/

void play_short_beep() {
  zowi._tone(150, 450, 100);
}

void play_long_beep() {
  zowi._tone(1000, 900, 100);
}


/*** Animation functions ***/

void countdown_animation() {
  zowi.putMouth(three);
  play_short_beep();
  delay(700);
  
  zowi.putMouth(two);
  play_short_beep();
  delay(700);
  
  zowi.putMouth(one);
  play_short_beep();
  delay(700);
    
  zowi.putMouth(zero);
  play_long_beep();
  delay(500);
  
  zowi.putMouth(default_mouth);
  end_animation();
}

// Animation to state movement was correct
void correct_animation() {
  zowi.sing(S_happy_short);
  end_animation();
}

// Animation to state sequence was correct
void win_animation() {
  zowi.sing(S_superHappy);
  zowi.putMouth(heart);
  zowi.swing(1,800,20); 
  zowi.sing(S_happy);
  zowi.swing(1,800,20);
  end_animation();
}

// Animation to state movement was wrong
void fail_animation() {
  zowi.putMouth(xMouth);
  zowi.sing(S_sad);
  int sadPos[4] = {110, 70, 20, 160};
  zowi._moveServos(700, sadPos); 
  delay(500);
  end_animation();
  
}

// Resets initial position and sends completion msg
void end_animation(){
  zowi.putMouth(default_mouth);
  zowi.home();
  send_movement_ended();
}


/*** Faces functions ***/

void happy_face() {
  zowi.putMouth(smile);
  end_face();
}

void angry_face() {
  zowi.putMouth(angry);
  end_face();
}

void sad_face() {
  zowi.putMouth(sadClosed);
  end_face();
}

// Resets face to original shape and sends movement completion msg
void end_face() {
    if (play_sound) {
      delay(face_delay_sound);
      // Play movement as end of sequence move
      zowi.sing(S_happy_short);
    }
    zowi.putMouth(default_mouth);
    send_movement_ended();
}


/***   Setup  ***/
void setup(){
  zowi.init();
  zowi.putMouth(smile);
}


/***   Loop  ***/
void loop(){

  // Enter if data available
  while (Serial.available() > 0) {
    String inString = Serial.readStringUntil('\n');
    
    // Choose movement according to read input
    if (inString.equals(WALK_FORWARD)) {
      walk_forwards();
    }
    else if (inString.equals(WALK_BACKWARDS)) {
      walk_backwards();
    }
    else if (inString.equals(WALK_LEFT)) {
      walk_left();
    }
    else if (inString.equals(WALK_RIGHT)) {
      walk_right();
    }
    else if (inString.equals(HAPPY_FACE)) {
      happy_face(); 
    }
    else if (inString.equals(RAISE_LEFT)) {
      raise_left();
    }
    else if (inString.equals(RAISE_RIGHT)) {
      raise_right();
    }
    else if (inString.equals(SAD_FACE)) {
      sad_face();
    }
    else if (inString.equals(ANGRY_FACE)) {
      angry_face();
    }
    else if (inString.equals( COUNTDOWN_ANIM)) {
      countdown_animation();
    }
    else if (inString.equals(CORRECT_ANIM)) {
      correct_animation();
    }
    else if (inString.equals(WIN_ANIM)) {
      win_animation();
    }
    else if (inString.equals(FAIL_ANIM)) {
      fail_animation();
    }
    else  {
      zowi.putMouth(xMouth);
    }

    delay(500);
  }
  
}




