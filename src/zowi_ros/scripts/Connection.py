#!/usr/bin/env python

import serial
from serial import SerialException

from utils import *
import time
import subprocess

from Movements import *
from Sequences import Sequence

logger = initialize_logging('pairing')


class ZowiManager(object):


    """ Binds to a serial port and sends Zowi commands """

    def __init__(self, port, baudrate=115200):
        self.port = port
        self.baudrate = baudrate
        self.serial = None

    def connect(self):
        "Establishes connection to specified device"

        # DEBUG: for usb only
        #self._restart_bluetooth()

        try:
            logger.info("Connecting to serial")
            self.serial = serial.Serial(self.port, self.baudrate)
            if not self.serial.isOpen():
                logger.info("Trying to open ...")
                self.serial.open()
            else:
                logger.info("Was already open. No need to open serial")

        except SerialException as ex:
            logger.error('Error connecting to port %s (%d) using serial: %s'
                         % (self.port, self.baudrate, str(ex)))


    def is_connected(self):
        """ Returns whether Zowi is connected to the serial interface """
        return self.serial.isOpen()


    def reconnect(self, num_times = 3):
        """ Tries to reconnect to the bluetooth device """
        tries = 0
        while not self.is_connected() and tries < num_times:
            self.connect()
            tries += 1

        if self.is_connected():
            logger.info("Could connect properly to %s (%d)" % (self.addr, self.port))
        else:
            logger.info("Could not connect to %s (%d). Aborting ..." % (self.addr, self.port))


    def _restart_bluetooth(self):
        """ Restarts the bluetooth service """
        subprocess.call(['sudo', '/etc/init.d/bluetooth', 'restart'])


    def wait_move_completion(self):
        """ Blocks on the serial interface waiting for the move completion.
         Returns True if everything went well, False otherwise"""
        msg = self.serial.readline()
        logger.info("Waiting for Zowi's movement completion...")
        while msg != MOVEMENT_END_MSG:
            logger.info('Received {} from serial'.format(msg))
            msg = self.serial.readline()


    def send_sequence(self, seq):
        """ Sends a list of movements to Zowi in one shot"""
        logging.info("Sending sequence to Zowi")
        for i, m in enumerate(seq):
            logger.info("Action %d: %s" % (i, m))
            self._send_raw(to_command(m))
            time.sleep(SLEEP_BETWEEN_MOVES)


    def send_random_sequence(self, length):
        """ Sends a random sequence of movements of given size to Zowi """
        logging.info("Generating random sequence")
        seq = Sequence()
        seq.generate_random(length)
        self.send_sequence(seq.get_movements())


    def play_init_seq_animation(self, wait=True):
        """ Plays a sequence with a countdown to the initial animation """
        self._send_raw(to_command(SpecialCommands.COUNTDOWN_ANIM), wait)


    def play_fail_animation(self, wait=True):
        """ Plays animation telling movement was correct """
        self._send_raw(to_command(SpecialCommands.FAIL_ANIM), wait)


    def play_correct_animation(self, wait=True):
        """ Plays animation telling movement was correct """
        self._send_raw(to_command(SpecialCommands.CORRECT_ANIM), wait)


    def play_sequence_correct(self, wait=True):
        """ Plays sound telling whole sequence was correct """
        self._send_raw(to_command(SpecialCommands.WIN_ANIM), wait)


    def _send_raw(self, m, wait=True):
        """ Sends raw bytes to Zowi"""
        if self.serial is None or not self.is_connected():
            logger.info("Unconnected. Trying to reconnect ... ")
            self.reconnect()
        self.serial.write(m)

        if wait:
            self.wait_move_completion()


    def finalize(self):
        """ Finish serial connection """
        self.serial.close()
