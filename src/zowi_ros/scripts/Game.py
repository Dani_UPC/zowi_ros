#!/usr/bin/env python

import rospy
from std_msgs.msg import String

from Sequences import Sequence
from Connection import ZowiManager
from Movements import *
from utils import *
import time


logger = initialize_logging('game')


class GameState(object):

    NOT_STARTED = 'not_init'  # User has not asked to start game
    PLAYING = 'playing'  # Zowi plays to user/user waits to start
    RECORDING = 'recording'  # User is mimicking Zowi
    SPEECH_TOPIC = 'speech' # TODO

class GameMessages(object):

    START_GAME = 'start_game'  # Game starts from scratch

    # Send by Kinect when the user has asked the system to start mimic Zowi
    # Kinect should start recording movements from this point
    START_SEQUENCE = 'start_sequence'

    # Message send to Kinect meaning that Zowi is playing a sequence
    # This disables body tracking until the user asks for the sequence to start (START_SEQUENCE)
    ZOWI_PLAYS = 'zowi_plays'

    END_GAME = 'end_game'  # Sets game to starting context

    FAILURE = 'fail'  # DEBUG only

    SUCCESS = 'success'  # DEBUG only

    WIN = 'win'


class Game(object):

    NODE_NAME = 'python_zowi'
    SUBSCRIBER_NAME = 'python_chatter'
    SUBSCRIBED_TO = 'kinect_chatter'
    SPEECH_TOPIC = 'speech' # TODO

    def __init__(self, port, baudrate=115200, starting_diff=2, queue_size=10, ros_rate=10, max_diff=2):
        """ Initializes owi imitation game
        Args:
            port: port to connect serial
            baudrate: Rate to use for serial messaging
            queue_size: ROS queue size
            ros_rate: Ros rate
            max_diff: Maximum difference at any point of a sequence between opposite walking moves
        """
        self.port = port
        self.baudrate = baudrate
        self.queue_size = queue_size
        self.ros_rate = ros_rate
        self.max_diff = max_diff
        self.reference_sequence = None
        self.manager = None
        self.ptr = 0
        self.state = GameState.NOT_STARTED
        self.starting_difficulty = starting_diff
        self.difficulty = starting_diff
        self.publisher, self.speech_pub = None, None


    def initialize(self):
        """ Starts connecting to Zowi """
        self.manager = ZowiManager(port=self.port, baudrate=self.baudrate)
        self.manager.connect()

        # Make sure it is connected
        if not self.manager.is_connected():
            self.manager.reconnect(num_times=3)

        if not self.manager.is_connected():
            raise RuntimeError("Could not connect to Zowi")

        self._ros_init()


    def _ros_init(self):
        """ Initializes subscribers and publishers for ROS"""
        # Subscribe to game events
        rospy.Subscriber(self.SUBSCRIBED_TO, String, self.command_received)
        # Publish to game sensoring system
        self.publisher = rospy.Publisher(self.SUBSCRIBER_NAME, String, queue_size=self.queue_size)
        # Publish for speech recognition
        self.speech_pub = rospy.Publisher(self.SPEECH_TOPIC, String, queue_size=self.queue_size)
        rospy.init_node(self.NODE_NAME)


    def run(self):
        """ Starts running the game """
        rate = rospy.Rate(self.ros_rate)
        while not rospy.is_shutdown():
            rate.sleep()


    def start_recording(self):
        """ Object to start receiving commands from Kinect"""
        self.state = GameState.RECORDING


    def generate_new_sequence(self):
        """ Generates a new sequence and sends it to Zowi """
        # Generate and store sequence
        self.reference_sequence = Sequence(max_walks=self.max_diff)
        self.reference_sequence.generate_random(self.difficulty)
        # Set pointer to received mimic
        self.ptr = 0

        rospy.loginfo(rospy.get_caller_id() + ". Generating new sequence of difficulty %d ..." % self.difficulty)


    def command_received(self, data):
        """ Event triggered when string received from remote node """
        # Parse ros message
        msg = data.data
        rospy.loginfo(rospy.get_caller_id() + ". Received message from Kinect: %s" % msg)

        if self.state == GameState.NOT_STARTED and msg == GameMessages.START_GAME:
            self.start_new_game()

        elif self.state == GameState.PLAYING and msg == GameMessages.START_SEQUENCE:
            rospy.loginfo(rospy.get_caller_id() + ". Set state to recording. Ready to receive commands ...")
            self.start_recording()

        elif self.state != GameState.NOT_STARTED and msg == GameMessages.END_GAME:
            rospy.loginfo(rospy.get_caller_id() + ". Quitting game...")
            self.finish_game()

        elif self.state == GameState.RECORDING and is_movement(msg):

            # Check if correct at current sequence position
            correct = self.reference_sequence.matches(msg, self.ptr)

            rospy.loginfo(rospy.get_caller_id() + ". Current movement is %s, received %s"
                          % (self.reference_sequence.get_move(self.ptr), msg))

            # If correct, increase difficulty and start new one
            if correct:
                rospy.loginfo(rospy.get_caller_id() + ". Correct movement spotted")
                # Play correct sound
                self.manager.play_correct_animation()
                # Increase pointer
                self.ptr += 1

                # Check if reached end of sequence
                if len(self.reference_sequence) == self.ptr:
                    # DEBUG ONLY
                    self.publisher.publish(GameMessages.WIN)

                    self.sequence_ended()
                else:
                    # DEBUG ONLY
                    self.publisher.publish(GameMessages.SUCCESS)

            else:

                # DEBUG ONLY
                self.publisher.publish(GameMessages.FAILURE)

                rospy.loginfo(rospy.get_caller_id() + ". Wrong movement")
                self.movement_failure()

        else:
            rospy.loginfo(
                rospy.get_caller_id() + ". Unconsistent state. State is %s and received %s" % (self.state, msg))


    def start_new_game(self):
        """ Starts new game: plays introduction, restarts difficulty and sends new sequence to Zowi """
        # Introduce game
        self.generate_speech(INTRO_TEXT)
        time.sleep(INTRO_SECONDS)
        # Starting with only two movements
        self.difficulty = self.starting_difficulty
        self.generate_new_sequence()
        rospy.loginfo(rospy.get_caller_id() + "Starting new game with difficulty %d ..." % self.difficulty)
        self.start_new_round()


    def finish_game(self):
        """ Finishes current game """
        self.state = GameState.NOT_STARTED


    def start_new_round(self):
        """ Sends movements to Zowi and notifies Kinect """
        # Set state as waiting to start mimicking
        self.state = GameState.PLAYING
        # Play speech + countdown + start sending sequence
        self.generate_speech(NEXT_LEVEL_TEXT)
        self.manager.play_init_seq_animation()
        self._send_sequence()
        # Notify Kinect player can start tracking
        self.publisher.publish(GameMessages.ZOWI_PLAYS)
        rospy.loginfo(rospy.get_caller_id() + ". Movements ended. Player can start round")


    def _send_sequence(self):
        """ Sends movement to Zowi and uses speech to identify it """
        for m in self.reference_sequence.get_movements():
            rospy.loginfo(rospy.get_caller_id() + ". Sending movement %s to Zowi" % m)
            self.generate_speech(to_speech(m))
            self.manager._send_raw(to_command(m))
        # Ready message to play
        self.generate_speech(READY_TEXT)


    def movement_failure(self):
        """ Resends current sequence when failure detected so Zowi remebers the moves to the user """
        # Send failure sound and wait
        self.manager.play_fail_animation()
        time.sleep(SLEEP_AFTER_FAILUE)
        self.start_new_round()


    def sequence_ended(self):
        """ Ends a sequence by playing victory sound, increasing level and sending next random sequence """
        # Play sound and increase difficulty
        self.manager.play_sequence_correct()
        self.difficulty += 1

        # Generate and send a new sequence
        self.generate_new_sequence()
        self.start_new_round()


    def generate_speech(self, text):
        """ Generates speech for the input text """
        self.speech_pub.publish(text)


    def finalize(self):
        self.manager.finalize()


if __name__ == '__main__':

    try:
        # g = Game(BLUETOOTH_PORT, BAUDRATE)
        g = Game('/dev/ttyUSB0', BAUDRATE, starting_diff=2)
        g.initialize()
        g.run()

    except rospy.ROSInterruptException as ex:
        print('Found error: %s' % ex)
        pass

    finally:
        g.finalize()
