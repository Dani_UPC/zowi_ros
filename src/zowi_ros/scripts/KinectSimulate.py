#!/usr/bin/env python

from Game import GameMessages, Game
from Movements import MovementType

import rospy
from std_msgs.msg import String

import time

class KinectStates(object):

    NOT_INIT = 'not_init'
    INIT = 'init'
    TRACKING = 'tracking'
    WAITING = 'waiting'


class Kinect(object):

    """ Class that simulates the Kinect behavior """

    NODE_NAME = 'kinect'


    def __init__(self, queue_size=10, rate=10):
        self.state = KinectStates.NOT_INIT
        self.queue_size = queue_size
        self.rate = rate
        self.publisher = None
        self.previous = None

        self._initialize_ros()


    def _initialize_ros(self):
        """ Creates subscriber and listener for the Zowi node """
        rospy.Subscriber(Game.SUBSCRIBER_NAME, String, self.command_received)
        self.publisher = rospy.Publisher(Game.SUBSCRIBED_TO, String, queue_size=self.queue_size)
        rospy.init_node(self.NODE_NAME)


    def run(self):
        rate = rospy.Rate(self.rate)
        self.publisher.publish(GameMessages.START_GAME)
        self.publisher.publish(GameMessages.START_SEQUENCE)
        while not rospy.is_shutdown():
            rate.sleep()


    def command_received(self, string):
        """ Command received handler """
        msg = string.data

        rospy.loginfo(rospy.get_caller_id() + '. Received %s', msg)

        if msg == GameMessages.ZOWI_PLAYS:
            rospy.loginfo(rospy.get_caller_id() + '. Waiting for user to send sequence start %s', msg)
            # Simulate user observing Zowi
            time.sleep(5)

            # Check if is first exchange
            if self.previous is not None:
                # Send start sequence
                self.publisher.publish(GameMessages.START_SEQUENCE)

            # Send random movement
            self.publisher.publish(MovementType.WALK_LEFT)

        elif msg == GameMessages.FAILURE:
            rospy.loginfo(rospy.get_caller_id() + '. Repetition failure %s', msg)
            self.previous = False

        elif msg == GameMessages.SUCCESS:
            rospy.loginfo(rospy.get_caller_id() + '. Repetition success %s', msg)
            self.previous = True
            # Send next random movement
            time.sleep(2)
            self.publisher.publish(MovementType.WALK_LEFT)

        elif msg == GameMessages.WIN:
            rospy.loginfo(rospy.get_caller_id() + '. Won game %s', msg)
            # Win! Game is over. Send start sequence after some wait
            time.sleep(2)
            self.publisher.publish(GameMessages.START_SEQUENCE)

        else:
            rospy.loginfo(rospy.get_caller_id() + '. Unknown message %s', msg)


if __name__ == '__main__':


    try:
        k = Kinect()
        k.run()

    except rospy.ROSInterruptException as ex:
        print('Found error: %s' % ex)
        pass
