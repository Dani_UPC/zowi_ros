#!/usr/bin/env python

from utils import *


class SpecialCommands(object):

    COUNTDOWN_ANIM = "countdown_anim"
    CORRECT_ANIM = "correct_anim"
    WIN_ANIM = "win_anim"
    FAIL_ANIM = "fail_anim"


class MovementType(object):

    WALK_RIGHT = 'walk_right'
    WALK_LEFT = 'walk_left'
    WALK_FORWARD = 'walk_forward'
    WALK_BACKWARDS = 'walk_backwards'
    HAPPY_FACE = 'happy_face'
    SAD_FACE = 'sad_face'
    ANGRY_FACE = 'angry_face'
    LEFT_ARM_UP = 'raise_left'
    RIGHT_ARM_UP = 'raise_right'


def is_movement(string):
    """ Whether the input string is a movement type"""
    return string in movement_map()


def movement_map():
    """ Map movements into a dictionary so each key is the string format and
    the value is the MovementType id """
    to_exclude = ['__module__', '__dict__', '__weakref__', '__doc__']
    return {getattr(MovementType, k): k for k in MovementType.__dict__.keys() if not k in to_exclude}


def get_movements_list():
    """ Returns the list of available movements """
    to_exclude = ['__module__', '__dict__', '__weakref__', '__doc__']
    return [getattr(MovementType, k) for k in MovementType.__dict__.keys() if not k in to_exclude]


def to_command(mov_type):
    """ Maps the input movement into a Zowi command """
    return mov_type + MOVEMENT_SEPARATOR


def to_speech(mov_type):
    """ Maps the input movement into a Zowi command """
    if mov_type == MovementType.WALK_RIGHT:
        return 'andar a la derecha'
    elif mov_type == MovementType.WALK_LEFT:
        return 'andar a la izquierda'
    elif mov_type == MovementType.WALK_FORWARD:
        return 'andar hacia delante'
    elif mov_type == MovementType.WALK_BACKWARDS:
        return 'andar hacia atras'
    elif mov_type == MovementType.SAD_FACE:
        return 'pon una cara triste'
    elif mov_type == MovementType.HAPPY_FACE:
        return 'pon una cara alegre'
    elif mov_type == MovementType.ANGRY_FACE:
        return 'pon una cara enfdadada'
    elif mov_type == MovementType.LEFT_ARM_UP:
        return 'brazo iquierdo arriba'
    elif mov_type == MovementType.RIGHT_ARM_UP:
        return 'brazo derecho arriba'
    else:
        raise ValueError("Unknown movement %s" % mov_type)
