
from Movements import *
from random import randint


class Sequence(object):


    def __init__(self, max_walks=2):
        self.movements = []
        self.max_walks = max_walks
        self.mov_list = get_movements_list()


    def generate_random(self, length):
        """ Generates a random sequence of given size so at any point i of the sequence, positions
        [0, i] follow that there difference between opposite moves (i.e. walk_forward vs walk_backwards and
        walk_left vs walk_right) is no more than max_walks

            e.g. [left, left, left, forward] would not be returned if max_walks = 2
            e.g. [left, right, left, left, forward] would be ok
        """
        self.movements = [self._get_random_move() for i in range(length)]
        self._check_sequence(self.movements)


    def _get_random_move(self):
        """ Returns a random move """
        total = len(self.mov_list)
        return self.mov_list[randint(0, total-1)]


    def _check_sequence(self, moves):
        """ Checks that the input sequence is valid so at any point, the difference between opposite
        walking types is at most max_walks """

        def check_pair(moves, move1, move2, max_diff):
            """ Balances pairs of movements """
            move1_counter, move2_counter = 0, 0
            for i, m in enumerate(moves):

                if m == move1:
                    move1_counter += 1
                elif m == move2:
                    move2_counter += 1

                # Check differences
                if abs(move1_counter - move2_counter) > max_diff:

                    if move1_counter > move2_counter:
                        # Current is move1 and made the difference. Balance
                        moves[i] = move2
                        move1_counter -= 1
                        move2_counter += 1
                    else:
                        # Current is move2 and made the difference. Balance
                        moves[i] = move1
                        move2_counter -= 1
                        move1_counter += 1

        check_pair(moves, MovementType.WALK_LEFT, MovementType.WALK_RIGHT, self.max_walks)
        check_pair(moves, MovementType.WALK_FORWARD, MovementType.WALK_BACKWARDS, self.max_walks)


    def generate_test(self, length):
        """ Generates a random sequence of given size with walk_left movement. Used for testing"""
        self.movements = [MovementType.WALK_LEFT] * length


    def get_movements(self):
        return self.movements


    def get_move(self, i):
        return self.movements[i]


    def matches(self, movement, pos):
        """ Returns whether the received command matches the input movement at given position"""
        return self.movements[pos] == movement


    def __len__(self):
        return len(self.movements)
