
import logging
import logging.config
import os

BLUETOOTH_PORT = '/dev/rfcomm0'
BAUDRATE = 115200

LOG_CONFIG = os.path.join(os.path.dirname(__file__), 'logs/logging.conf')
LOG_OUT = os.path.join(os.path.dirname(__file__), 'logs/output.log')

MOVEMENT_SEPARATOR = '\n'
SLEEP_BETWEEN_MOVES = 1
SLEEP_AFTER_FAILUE = 2

# Speeches
INTRO_TEXT = "Hola, mi amigo Zowi quiere jugar contigo. Para que Zowi se divierta deberas repetir sus movimientos." + \
            "Cuando acabe de hacerlos, tendras que decir listo para empezar a imitarlos. " + \
            "Cuidado, porque cada vez sera mas dificil. Empezamos."
INTRO_SECONDS = 10
READY_TEXT = "Cuando quieras"
NEXT_LEVEL_TEXT = "Siguiente nivel"

# Handshake msgs
MOVEMENT_END_MSG = 'ok\n'


def initialize_logging(name):
    """ Initializes logging with the default configuration and the
    output where to store the logs. Set to None to disable storing into file
     :param name: Name of the logger to initialize """
    logging.config.fileConfig(LOG_CONFIG, defaults={'logfilename': LOG_OUT})
    return logging.getLogger(name)
